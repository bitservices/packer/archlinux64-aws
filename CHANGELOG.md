<!----------------------------------------------------------------------------->

### **v2.0.13**
* **archlinx64 Image Version**: 2.0.13

<!----------------------------------------------------------------------------->

### **v2.0.12**
* **archlinx64 Image Version**: 2.0.12

<!----------------------------------------------------------------------------->

### **v2.0.11**
* **archlinx64 Image Version**: 2.0.11

<!----------------------------------------------------------------------------->

### **v2.0.10**
* **archlinx64 Image Version**: 2.0.10

<!----------------------------------------------------------------------------->

### **v2.0.9**
* **archlinx64 Image Version**: 2.0.9

<!----------------------------------------------------------------------------->

### **v2.0.8**
* **archlinx64 Image Version**: 2.0.8

<!----------------------------------------------------------------------------->

### **v2.0.7**
* **archlinx64 Image Version**: 2.0.7
* **Add**: Add `prometheus-node-exporter` package. - [Rich Lees]
* **Add**: Enabled the `prometheus-node-exporter.service` service. - [Rich Lees]
* **Change**: Disable all Prometheus Node Exporter collectors by default. - [Rich Lees]
* **Change**: Enable the following Prometheus Node Exporter collectors: `btrfs`, `cpu`, `cpufreq`, `diskstats`, `edac`, `entropy`, `filesystem`, `hwmon`, `loadavg`, 'mdadm', 'meminfo', 'netclass', 'netdev', 'netstat', 'nfs', 'nvme', 'pressure', 'stat', 'thermal_zone' and 'udp_queues'. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v2.0.6**
* **archlinx64 Image Version**: 2.0.6
* **Change**: Use `aws-cli-v2-bits` package instead since `aws-cli-v2` has been demoted to the AUR. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v2.0.5**
* **archlinx64 Image Version**: 2.0.5

<!----------------------------------------------------------------------------->

### **v2.0.4**
* **archlinx64 Image Version**: 2.0.4

<!----------------------------------------------------------------------------->

### **v2.0.3**
* **archlinx64 Image Version**: 2.0.3

<!----------------------------------------------------------------------------->

### **v2.0.2**
* **archlinx64 Image Version**: 2.0.2

<!----------------------------------------------------------------------------->

### **v2.0.1**
* **archlinx64 Image Version**: 2.0.1

<!----------------------------------------------------------------------------->

### **v2.0.0**
* **archlinx64 Image Version**: 2.0.0

<!----------------------------------------------------------------------------->

### **v1.0.3**
* **archlinx64 Image Version**: 1.0.3
* **Add**: Add `aws-cli-v2` package. - [Rich Lees]
* **Change**: Put EC2 NTP configuration into `timesyncd.conf.d` folder. No tangible change. - [Rich Lees]
* **Remove**: Remove `aws-cli` package. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v1.0.2**
* **archlinx64 Image Version**: 1.0.2

<!----------------------------------------------------------------------------->

### **v1.0.1**
* **archlinx64 Image Version**: 1.0.1

<!----------------------------------------------------------------------------->

### **v1.0.0**
* **archlinx64 Image Version**: 1.0.0

<!----------------------------------------------------------------------------->

### **v0.4.1**
* **archlinx64 Image Version**: 0.4.1
* **Change**: Revert system volume size back to 3 GB. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v0.4.0**
* **archlinx64 Image Version**: 0.4.0
* **Change**: Increase system volume size to 5 GB. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v0.3.7**
* **archlinx64 Image Version**: 0.3.7

<!----------------------------------------------------------------------------->

### **v0.3.6**
* **archlinx64 Image Version**: 0.3.6

<!----------------------------------------------------------------------------->

### **v0.3.5**
* **archlinx64 Image Version**: 0.3.5

<!----------------------------------------------------------------------------->

### **v0.3.4**
* **archlinx64 Image Version**: 0.3.4

<!----------------------------------------------------------------------------->

### **v0.3.3**
* **archlinx64 Image Version**: 0.3.3

<!----------------------------------------------------------------------------->

### **v0.3.2**
* **archlinx64 Image Version**: 0.3.2

<!----------------------------------------------------------------------------->

### **v0.3.1**
* **archlinx64 Image Version**: 0.3.1

<!----------------------------------------------------------------------------->

### **v0.3.0**
* **archlinx64 Image Version**: 0.3.0
* **New**: Use the built-in Amazon Web Services NTP server. - [Rich Lees]
* **Change**: Change AMI name format, to allow easier filtering on kernel name. - [Rich Lees]
* **Change**: Include architecture in the AMI name. - [Rich Lees]
* **Remove**: No longer provide Artemis images.

<!----------------------------------------------------------------------------->

### **v0.2.27**
* **archlinx64 Image Version**: 0.2.27
* **Artemis Version**: v3.0.0

<!----------------------------------------------------------------------------->

### **v0.2.26**
* **archlinx64 Image Version**: 0.2.26
* **Artemis Version**: v3.0.0

<!----------------------------------------------------------------------------->

### **v0.2.25**
* **archlinx64 Image Version**: 0.2.25
* **Artemis Version**: v3.0.0

<!----------------------------------------------------------------------------->

### **v0.2.24**
* **archlinx64 Image Version**: 0.2.24
* **Artemis Version**: v3.0.0

<!----------------------------------------------------------------------------->

### **v0.2.23**
* **archlinx64 Image Version**: 0.2.23
* **Artemis Version**: v3.0.0

<!----------------------------------------------------------------------------->

### **v0.2.22**
* **archlinx64 Image Version**: 0.2.22
* **Artemis Version**: v3.0.0
* **Change**: Artemis now uses the **rlees85** fork. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v0.2.21**
* **archlinx64 Image Version**: 0.2.21
* **Artemis Version**: archlinux (unreleased)

<!----------------------------------------------------------------------------->

### **v0.2.20**
* **archlinx64 Image Version**: 0.2.20
* **Artemis Version**: archlinux (unreleased)
* **New**: Include `aws-cli` package. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v0.2.19**
* **archlinx64 Image Version**: 0.2.19
* **Artemis Version**: archlinux (unreleased)
* **Change**: Amend `ebsnvme-id` script to work with **Python 3**. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v0.2.18**
* **archlinx64 Image Version**: 0.2.18
* **Artemis Version**: archlinux (unreleased)
* **Change**: For all AMIs the primary hard disk size has been increased to `3GB` by default. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v0.2.17**
* **archlinx64 Image Version**: 0.2.17
* **Artemis Version**: archlinux (unreleased)

<!----------------------------------------------------------------------------->

### **v0.2.16**
* **archlinx64 Image Version**: 0.2.16
* **Artemis Version**: archlinux (unreleased)

<!----------------------------------------------------------------------------->

### **v0.2.15**
* **archlinx64 Image Version**: 0.2.15
* **Artemis Version**: archlinux (unreleased)

<!----------------------------------------------------------------------------->

### **v0.2.14**
* **archlinx64 Image Version**: 0.2.14
* **Artemis Version**: archlinux (unreleased)
* **Change**: Replace `growfs` mkinitcpio hook with `growrootpart`

<!----------------------------------------------------------------------------->

### **v0.2.13**
* **archlinx64 Image Version**: 0.2.13
* **Artemis Version**: archlinux (unreleased)

<!----------------------------------------------------------------------------->

### **v0.2.12**
* **archlinx64 Image Version**: 0.2.12
* **Artemis Version**: archlinux (unreleased)

<!----------------------------------------------------------------------------->

### **v0.2.11**
* **archlinx64 Image Version**: 0.2.11
* **Artemis Version**: v2.1.0-archlinux
* **Change**: Artemis AMIs now have a much more sensible **login.defs** UMASK `077` - [Rich Lees]
* **Change**: Artemis AMIs now have the timezone set to `UTC` - [Rich Lees]
* **Bugfix**: Fixed bug where Artemis AMIs have IPv6 router advertisements disabled - [Rich Lees]
    * sysctl parameter `net.ipv6.conf.all.accept_ra` now set to **1**
    * sysctl parameter `net.ipv6.conf.default.accept_ra` now set to **1**

<!----------------------------------------------------------------------------->

### **v0.2.10**
* **archlinx64 Image Version**: 0.2.10
* **Artemis Version**: v2.1.0-archlinux
* **New**: Include Amazon EBS friendly name rules for NVMe disks (EC2 AMIs only) - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v0.2.9**
* **archlinx64 Image Version**: 0.2.9
* **Artemis Version**: v2.1.0-archlinux

<!----------------------------------------------------------------------------->

### **v0.2.8**
* **archlinx64 Image Version**: 0.2.8
* **Artemis Version**: v2.1.0-archlinux
* **Change**: **Artemis** AMIs now configure NTPD to use the AWS timeserver: `169.254.169.123`. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v0.2.7**
* **archlinx64 Image Version**: 0.2.7
* **Artemis Version**: v2.1.0-archlinux

<!----------------------------------------------------------------------------->

### **v0.2.6**
* **archlinx64 Image Version**: 0.2.6
* **Artemis Version**: v2.1.0-archlinux

<!----------------------------------------------------------------------------->

### **v0.2.5**
* **archlinx64 Image Version**: 0.2.5
* **Artemis Version**: v2.1.0-archlinux

<!----------------------------------------------------------------------------->

### **v0.2.4**
* **archlinx64 Image Version**: 0.2.4
* **Artemis Version**: v2.1.0-archlinux

<!----------------------------------------------------------------------------->

### **v0.2.3**
* **archlinx64 Image Version**: 0.2.3
* **Artemis Version**: v2.1.0-archlinux
* **Change**: All AMIs have flag set for **SR-IOV** and **Amazon ENA** support. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **v0.2.2**
* **archlinx64 Image Version**: 0.2.2
* **Artemis Version**: v2.1.0-archlinux

<!----------------------------------------------------------------------------->

### **v0.2.1**
* **archlinx64 Image Version**: 0.2.1
* **Artemis Version**: v2.0.0-archlinux

**Note**: With this release, due to Artemis not having a release yet that is compatible with
OpenSSH 7.6, the Artemis AMIs with this release are limited (by overrides) to using the following
SSH daemon MACs:

* hmac-sha2-512-etm@openssh.com
* hmac-sha2-256-etm@openssh.com

<!----------------------------------------------------------------------------->

### **v0.2.0**
* **archlinx64 Image Version**: 0.2.0
* **Artemis Version**: v2.0.0-archlinux
* **New**: EC2 AMIs are now provided in `eu-west-1` and `eu-west-2` regions. - [Rich Lees]
* **New**: EC2 with [Artemis](https://github.com/travisperkins/artemis) AMIs are now provided in `eu-west-1` and `eu-west-2` regions with the following security modes: `standard`, `balanced` and `enhanced`. - [Rich Lees]
* **New**: For all AMIs the primary hard disk is now `2GB` by default. - [Rich Lees]

<!----------------------------------------------------------------------------->

[Rich Lees]: https://gitlab.com/rlees85
